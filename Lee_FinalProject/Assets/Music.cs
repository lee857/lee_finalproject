﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour {

    AudioSource aS;
	// Use this for initialization
	void Start () {
        aS = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.M))
        {
            aS.Stop();
        }

        aS.volume -= Time.deltaTime * .1f;
	}
}
