﻿using UnityEngine;
using System.Collections;


public class GemHover : MonoBehaviour {

	float startY;
	float hover = 0.1f;
	float speed = 2f;
    
	void Start () {
		startY = transform.position.y;
	}
	
	void Update () {
		Vector3 position = transform.position;
		position = new Vector3 (position.x, startY + Mathf.Sin(Time.time*speed)*0.1f, position.z);
		transform.position = position;
	}
}
