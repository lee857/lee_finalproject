﻿using UnityEngine;
using System.Collections;


public class CameraScript : MonoBehaviour {
    
	public Transform target;

	Vector3 oldPosition;
    
	void Start () {
		setup ();
	}
    
	public void setup() {
		if (target == null)
			target = GameObject.Find ("Sphere").transform;
		oldPosition = target.position;
	}
    
	void Update () {
	
	}
    
	void LateUpdate() {
		if (target != null) {
			Vector3 position = target.position;
			Vector3 delta = oldPosition - position;
			delta.y = 0;
			transform.position = transform.position - delta;
			oldPosition = position;
		}
	}
}
