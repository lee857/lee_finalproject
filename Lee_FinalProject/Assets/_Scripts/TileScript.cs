﻿using UnityEngine;
using System.Collections;


public class TileScript : MonoBehaviour {
    
	Vector3 startMarker;
	Vector3 endMarker;
	public float speed = 3.0F;
	private float startTime;
	private float journeyLength;
    
	public GameObject gem;

	bool triggerLeft;

	float timer;
	float timerInterval = 0.2f;
    
	void Start () {
		triggerLeft = false;
		startMarker = transform.position;
		endMarker = startMarker - new Vector3 (0, 10, 0);
        
		int myRand = Random.Range (0, 101);
		if (myRand < 40 && gameObject.name != "Plane") {
			GameObject gemGO=Instantiate(gem,transform.position,Quaternion.identity) as GameObject;
			gemGO.transform.position += new Vector3(0,2,0);
			gemGO.transform.SetParent(this.transform);
		}
	}
	
	void Update () {
		if (triggerLeft) {
			timer += Time.deltaTime;
			float distCovered = (timer-timerInterval)*speed;
				float fracJourney = distCovered / journeyLength;
				transform.position = Vector3.Lerp (startMarker, endMarker, fracJourney);
				if (fracJourney > 0.98f)
					Destroy(this.gameObject);
		}
	}
    
	void OnTriggerExit(Collider other) {
		if (other.name == "Sphere") {
			startTime = Time.time;
			journeyLength = Vector3.Distance(startMarker, endMarker);
			triggerLeft = true;
		}
	}
}