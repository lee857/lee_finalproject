﻿using UnityEngine;
using System.Collections;


public class PlayerController : MonoBehaviour
{
    
	Rigidbody rigidBody;
    bool rollingLeft;
	bool fallingDown;
	public float speed=2;
    public float increase = 0.1f;
    AudioSource turn;
    bool sound;
    
	GameScript gameScriptReference;
    
	void Start () {
		rigidBody = GetComponent<Rigidbody> ();
		rollingLeft = true;
		fallingDown = false;
        sound = false;
		gameScriptReference = GameObject.Find ("GameController").GetComponent<GameScript> ();
        turn = GetComponent<AudioSource>();
	}

    
	void Update () {
		if (gameScriptReference.inGame()) {
			if (Input.GetKeyDown (KeyCode.Space))
            {
				gameScriptReference.addScore(1);
				rollingLeft = !rollingLeft;
                speed += increase * Time.deltaTime;
                turn.Play();

            }
			if (transform.position.y < -10) {
				gameScriptReference.gameOver();
				Destroy(gameObject);
			}
			if (!fallingDown) {
				if (transform.position.y < -0.3f) {
					fallingDown = true;
				}
			}
		}
	}


	void FixedUpdate() {
		if (gameScriptReference.inGame()) {
			if (rollingLeft)
				rigidBody.velocity = new Vector3 (-speed, Physics.gravity.y, 0);
			else
				rigidBody.velocity = new Vector3 (0, Physics.gravity.y, speed);
		}
	}
}
