﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class GameScript : MonoBehaviour
{
    public AudioSource[] sounds;
    public AudioSource aS;
    bool bgm;
    public AudioSource item;
    public AudioSource over;

	Vector3 startPosition;
	Vector3 actualPosition;
	int myRandom;
    
	public GameObject tile;
	public GameObject plane;
	public GameObject sphere;
	public GameObject menuPanel;
	public GameObject gameOverPanel;
    
	public Text scoreText;
	public Text scoreText1;
	public Text hiScoreText1;

	public Text menuBestScore;
	public Text menuGamesPlayed;

	float timer;
	float timerInterval = 0.3f;

	int score;
    

    public Text timeText;

    private float remaining;

    private float sec;

    public enum GameState {
		menu, 
		game,
		over,
	};

	public GameState gameState;
    

	public Light dayLight;
	public Material tileMat;

	void Awake() {
		setupGame ();
		menuPanel.SetActive (true);
		gameOverPanel.SetActive (false);
	}
    
	void Start () {
		if (!PlayerPrefs.HasKey ("HiScore"))
			PlayerPrefs.SetInt ("HiScore", 0);
		if (!PlayerPrefs.HasKey ("GamesPlayed"))
			PlayerPrefs.SetInt ("GamesPlayed", 0);

        remaining = 15f;
        sounds = GetComponents<AudioSource>();
        aS = sounds[0];
        item = sounds[1];
        over = sounds[2];
        bgm = true;
    }

    
	void Update () {
        if (Input.GetKeyDown(KeyCode.M))
        {
            if (bgm == true)
            {
                aS.Stop();
                bgm = false;
            }
            else
            {
                aS.Play();
                bgm = true;
            }
        }
        

		switch (gameState) {

		case GameState.menu:
			break;

		case GameState.game:

                remaining -= Time.deltaTime;
                if (remaining > 0)
                {
                    sec = (remaining % 60f);
                    timeText.text = "Time: " + sec.ToString("00") + " seconds";
                }
                else
                {
                    gameOver();
                }

                timer += Time.deltaTime;
			if (timer >= timerInterval)
                {
				timer -= timerInterval;
				buildTile ();
			    }
			break;
		case GameState.over:
			break;

		}
	}
    
	void setupGame() {
		score = 0;
		scoreText.text = score.ToString ("D5");
		scoreText1.text = score.ToString ("D5");

		menuBestScore.text = "BEST SCORE: " + PlayerPrefs.GetInt ("HiScore").ToString();
		menuGamesPlayed.text = "GAMES PLAYED: " + PlayerPrefs.GetInt ("GamesPlayed").ToString();
        
		GameObject[] tiles = GameObject.FindGameObjectsWithTag ("Tile");
		foreach (GameObject t in tiles)
			Destroy (t);
		GameObject planeGO = Instantiate (plane, new Vector3 (0, -2, 0), Quaternion.identity) as GameObject;
		planeGO.name = "Plane";
		GameObject sphereGO = Instantiate (sphere, new Vector3 (0, -0.1f, 0), Quaternion.Euler(45,63,23)) as GameObject;
		sphereGO.name = "Sphere";

		// set the camera object
		Camera.main.transform.position = new Vector3 (5, 5, -5);
		Camera.main.GetComponent<CameraScript> ().setup ();
        
		startPosition = new Vector3(-2,-2,3);
		GameObject newTile=Instantiate(tile,startPosition,Quaternion.identity) as GameObject;
		newTile.name = "Tile";
		actualPosition = startPosition;
		for (int i = 0; i < 20; i++) {
			buildTile ();
		}
	}
    
	void buildTile() {
		Vector3 newPosition = actualPosition; 
		myRandom = Random.Range(0,101);
		if (myRandom < 50) {
			newPosition.x -= 1;
		}
		else {
			newPosition.z += 1;
		}
		actualPosition = newPosition;
		GameObject newTile=Instantiate(tile,actualPosition,Quaternion.identity) as GameObject;
		newTile.name = "Tile";
		newTile.tag = "Tile";
	}
    
	public bool inGame() {
		if (gameState == GameState.game)
			return true;
		else
			return false;
	}
    
	public void addScore(int amount) {
		score += amount;
		scoreText.text = score.ToString ("D5");
		scoreText1.text = score.ToString ("D5");
	}
    public void addTime(float num)
    {
        remaining += num;
        item.Play();
    }

    public void gameOver() {
		gameState = GameState.over;
		gameOverPanel.SetActive (true);
		int hiScore = PlayerPrefs.GetInt ("HiScore");
		if (score > hiScore)
			PlayerPrefs.SetInt ("HiScore", score);

		scoreText1.text = score.ToString ("D5");
		hiScore = PlayerPrefs.GetInt ("HiScore");
		hiScoreText1.text = hiScore.ToString ("D5");

		int gamesPlayed = PlayerPrefs.GetInt ("GamesPlayed");
		gamesPlayed++;
		PlayerPrefs.SetInt ("GamesPlayed", gamesPlayed);
        over.Play();
        aS.Stop();
        bgm = false;
	}

    

	public void playGame() {
		menuPanel.SetActive (false);
		gameState = GameState.game;
	}
 
	public void resumeGame() {
		if (Time.timeScale == 0) {
			Time.timeScale = 1;
			gameState = GameState.game;
		} 
		else {
			if (GameObject.Find("Plane") == null)
				setupGame();
			menuPanel.SetActive (true);
			gameState = GameState.menu;
		}
	}

	public void retryGame() {
		setupGame ();
		menuPanel.SetActive (true);
		gameOverPanel.SetActive (false);
		gameState = GameState.menu;
        remaining = 30f;
        aS.Play();
        bgm = true;
	}
    
}
