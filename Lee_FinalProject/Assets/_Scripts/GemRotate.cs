﻿using UnityEngine;
using System.Collections;


public class GemRotate : MonoBehaviour {

	float speed = 1f;
	float angle;
    
	void Start () {
	
	}
	
	void Update () {
		angle = (angle + speed) % 360f;
		transform.localRotation = Quaternion.Euler(new Vector3(0,angle,0));
	}
}
