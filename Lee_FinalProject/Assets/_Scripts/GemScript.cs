﻿using UnityEngine;
using System.Collections;


public class GemScript : MonoBehaviour
{
    
	GameScript gameScriptReference;
    
	public ParticleSystem sparkle;
    AudioSource item;
    bool sound;

    void Start () {
		gameScriptReference = GameObject.Find ("GameController").GetComponent<GameScript> ();
        item = GetComponent<AudioSource>();
        sound = false;
    }
	
	void Update () {
	    if (sound)
        {
            item.Play();
        }
	}
    
	void OnTriggerEnter(Collider other) {
		if (other.name == "Sphere") {
            sound = true;
            gameScriptReference.addScore(5);
            gameScriptReference.addTime(1);
			ParticleSystem particleSystem = Instantiate(sparkle, transform.position, Quaternion.identity) as ParticleSystem;
			this.gameObject.SetActive(false);
        }
	}
}
